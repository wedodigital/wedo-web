var $ = jQuery.noConflict();
$(document).ready(function() {

    var windowHeight = $(window).height();

    jQuery('h1 span').typed({
        strings: ['design', '<span class="small">development</span>', 'branding', 'ecommerce', 'ux', 'digital'],
        typeSpeed: 50,
        callback: function() {
            $('h1').addClass('done');
            $('.button.hidden').fadeIn();
        }
    });

});

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
